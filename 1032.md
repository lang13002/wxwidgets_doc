### 顶层窗口

顶层窗口直接被放置在桌面上而不是包含在其它窗口之内。如果应用程序允许，他们可以被移动或者重新改变大小。


总共有三种基础的顶层窗口类型：wxFrame和wxDialog都是从一个虚类wxTopLevelWindow继承来的，一个dialog既可以是模式的也可以是非模式的，而frame通常都是非模式的。另外一个wxPopupWindow功能相对简单，是直接从wxWindow继承过来的。


模式对话框的意思是说当这个对话框弹出时，应用程序除了等待用户关闭这个对话框以外不再作别的事情。对于那些要等待用户响应以后才能继续的操作来说。这是比较合适的。但是寻找替代的解决方案通常也不是一件坏事。举例来说，在工具条上设置一个字体选项可能比弹出一个模式的对话框让用户选择字体更能是整个应用程序看上去更流畅。

顶层窗口通常都拥有一个标题栏，这个标题栏上有一些按钮或者菜单或者别的修饰用来关闭，或者最小化，或者恢复这个窗口。而frame窗口则通常还会拥有菜单条，工具条和状态条。但是通常对话框则没有这些。在Mac OS X上，frame窗口的菜单条通常不是显示在frame窗口的顶部而是显示在整个屏幕的顶部。

不要被"顶层窗口"的叫法和wxApp::GetTopWindow函数给搞糊涂了。wxWidgets使用后者来得到应用程序的主窗口，这个主窗口通常是你在应用程序里创建的第一个frame窗口或者dialog窗口。

如果需要，你可以使用全局变量wxTopLevelWindows来访问所有的顶层窗口，这个全局变量是一个wxWindowList类型。

**wxFrame**

wxFrame是主应用程序窗口的一个通用的选择。下图演示了常见的各个frame窗口元素。frame窗口拥有一个可选的标题栏(上面有一些类似关闭功能的按钮)，一个菜单条，一个工具栏，一个状态栏。剩下的区域则称为客户区，如果拥有超过一个子窗口，那么他们的尺寸和位置是由应用程序决定的。你应该通过第7章中会讲到的窗口布局控件来作相关的事情。或者如果只有两个子窗口的话，你可以考虑使用一个分割窗口。后者我们稍后就会讲到。

![](images/0302.jpg)

在某些平台上不允许直接绘制frame窗口，因此你应该增加一个wxPanel作为容器来放置别的子窗口控件，以便可以通过键盘来遍历各个控件。

frame窗口是允许包含多个工具条的，但是它只会自动放置第一个工具条。因此你需要自己明确的布局另外的工具条。

你很应该基于wxFrame实现你自己的frame类而不是直接使用wxFrame类，并且在其构造函数中创建自己的菜单条和子窗口。这会让你更方便处理类似wxEVT_CLOSE事件和别的命令事件。

你可以给frame窗口指定一个图标以便让系统显示在任务栏上或者文件管理器中。在windows平台上，你最好指定一组16x16和32x32，并且拥有不同颜色深度的图标。在linux平台上也一样，windows系统上的图标已经足够。而在Max OsX上，你需要更多的不同颜色和深度的图标。关于图标和图标列表的更多信息，可以参考第10章，“在应用程序中使用图片”。

除了默认的构造函数以外，wxFrame还拥有下面的构造函数

```c++
wxFrame(wxWindow* parent,
        wxWindowID id,
        const wxString& title,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxDEFAULT_FRAME_STYLE,
        const wxString& name = wxT("frame")
       );
```

**例如：**

```c++
wxFrame* frame = new wxFrame(NULL, ID_MYFRAME, wxT("Hello wxWidgets"), wxDefaultPosition, wxSize(500, 300));
frame->Show(true);
```


注意在你显式的调用Show函数之前，frame是不会被显示的。以便应用程序有机会在所有的子窗口还没有被显示的时候进行窗口布局。

没有必要给这个新建的frame窗口指定父窗口。如果指定了父窗口并且指定了wxFRAME_FLOAT_ON_PARENT类型位，这个新建的窗口将会显示在它的父窗口的上层。

要删除一个frame窗口，应该使用Destroy方法而不是使用delete操作符，因为Destroy会在处理完这个窗口的所有事件以后才真正释放这个窗口。调用Close函数会导致这个窗口收到wxEVT_CLOSE事件，这个事件默认的行为是调用Destroy方法。当一个frame窗口被释放时。它的所有的子窗口都将被释放，谁叫他们自己不是顶层窗口呢。

当最后一个顶层窗口被释放的时候，应用程序就退出了(这种默认的行为可以通过调用wxApp::SetExitOnFrameDelete改变)。你应该在你的主窗口的wxEVT_CLOSE事件处理函数中调用Destroy释放其它的顶层窗口(比如：一个查找对话框之类)，否则可能出现主窗口已经关闭但是应用程序却不退出的情况。

frame窗口没有类似dialog窗口那样的wxDialog::ShowModal方法可以禁止其它顶层窗口处理消息，然而，还是可以通过别的方法达到类似的目的。一个方法是通过创建一个wxWindowDisabler对象，另外一个方法是通过wxModalEventLoop对象，传递这个frame窗口的指针作为参数，然后调用Run函数开始一个本地的事件循环，然后调用Exit函数(通常是在这个Frame窗口的某个事件处理函数里)退出这个循环。

下图演示了一个用户应用程序的frame窗口在windows上运行的样子。这个frame窗口拥有一个标题栏，一个菜单条，一个五彩的工具栏，在frame窗口的客户区有个分割窗口，底端有一个状态条，正显示着当用户的鼠标划过工具条上的按钮或者菜单项的时候显示的提示。

![](images/0303.jpg)

**wxFrame的窗口类型比特位**

Frame窗口比起基本窗口类，增加了下面一些类型：

| 窗口类型                | 说明                                                         |
| ----------------------- | ------------------------------------------------------------ |
| wxDEFAULT_FRAME_STYLE   | 其值为 wxMINIMIZE_BOX \| wxMAXIMIZE_BOX \| wxRESIZE_BORDER \| wxSYSTEM_MENU\| wxCAPTION \| wxCLOSE_BOX|
| wxICONIZE               | 以最小化的方式显示窗口。目前只适用于Windows平台。            |
| wxCAPTION               | 在窗口上显示标题。                                           |
| wxMINIMIZE              | 和wxICONIZE的意义相同。也只适用于Windows平台。               |
| wxMINIMIZE_BOX          | 显示一个最小化按钮。                                         |
| wxMAXIMIZE              | 以最大化方式显示窗口。仅适用于Windows。                      |
| wxMAXIMIZE_BOX          | 在窗口上显示最大化按钮。                                     |
| wxCLOSE_BOX             | 在窗口上显示关闭按钮。                                       |
| wxSTAY_ON_TOP           | 这个窗口显示在其它所有顶层窗口之上。仅适用于Windows。        |
| wxSYSTEM_MENU           | 显示系统菜单。                                               |
| wxRESIZE_BORDER         | 边框可改变大小。                                             |
| wxFRAME_TOOL_WINDOW     | 窗口的标题栏比正常情况要小，而且在windows平台上，这个窗口不会在任务栏上显示。 |
| wxFRAME_NO_TASKBAR      | 创建一个标题栏是正常大小，但是不在任务栏显示的窗口。目前支持windows和linux。需要注意在windows平台上，这个窗口最小化时将会最小化到桌面上，这有时候会让用户觉得不习惯，因此这种类型的窗口最好不要使用wxMINIMIZE_BOX类型。 |
| wxFRAME_FLOAT_ON_PARENT | 拥有这种类型的窗口总会显示在它的父窗口的上层。使用这种类型的窗口必须拥有非空父窗口，否则可能引发断言错误。 |
| wxFRAME_SHAPED          | 拥有这种类型的窗口可以通过调用SetShape函数来使其呈现不规则窗口外貌。 |

下表列出的是frame窗口的扩展类型，需要使用wxWindow::SetExtraStyle函数才可以设置扩展类型

| 窗口类型               | 说明                                                         |
| ---------------------- | ------------------------------------------------------------ |
| wxFRAME_EX_CONTEXTHELP | 在windows平台上，这个扩展类型导致标题栏增加一个帮助按钮。当这个按钮被点击以后，窗口会进入一种上下文帮助模式。在这种模式下，任何应用程序被点击时，将会发送一个wxEVT_HELP事件。 |
| wxFRAME_EX_METAL       | 在Mac OS X平台上，这个扩展类型将会导致窗口使用金属外观。请小心使用这个扩展类型，因为它可能会假定你的应用程序用户默认安装类声卡之类的多媒体设备。 |

**wxFrame的事件**

下表列出了frame窗口比基本窗口类增加的事件类型：

| 事件               | 说明                                                         |
| ------------------ | ------------------------------------------------------------ |
| EVT_ACTIVATE(func) | 用来处理wxEVT_ACTIVATE事件，在frame窗口被激活或者去激活的时候产生。处理函数的参数类型为wxActivateevent. |
| EVT_CLOSE(func)    | 用来处理wxEVT_CLOSE事件，在应用程序准备关闭窗口的时候产生。处理函数的参数类型是wxCloseevent，这个类型支持Veto函数调用以阻止这个事件的进一步处理。 |
| EVT_ICONIZE(func)  | 用来处理wxEVT_ICONIZE事件，当窗口被最小化或者被恢复普通大小的时候产生。处理函数的参数类型为wxIconizeevent，通过使用IsIconized函数来判断究竟是最小化事件还是恢复事件。 |
| EVT_MAXIMIZE(func) | 用来处理wxEVT_MAXIMIZE事件，当窗口被最大化或者从最大化恢复的时候产生。处理函数的参数类型为wxMaximizeevent，通过IsMaximized函数判断究竟是最大化还是从最大化状态恢复。 |

**wxFrame的成员函数**

下面将介绍wxFrame类的主要的成员函数。因为wxFrame类是从wxTopLevelWindow和wxWindow继承过来的，请同样参考这两个类的成员函数。

CreateStatusBar函数在frame窗口的底部创建一个拥有一个或多个域的状态栏。可以使用SetStatusText函数来设置状态栏上的文字，使用SetStatusWidths函数来设置状态栏每个域的宽度(参考本章稍后对wxStatusBar的介绍)。使用举例：

```c++
frame->CreateStatusBar(2, wxST_SIZEGRIP);
int widths[3] = { 100, 100, -1 };
frame->SetStatusWidths(3, widths);
frame->SetStatusText(wxT("Ready"), 0);
```

CreateToolBar函数在frame窗口的菜单条下创建一个工具栏。当然你也可以直接创建一个wxToolBar实例，然后调用wxFrame::SetToolBar函数以便让frame类来管理你刚创建的toolbar。

GetMenuBar 和 SetMenuBar用来操作frame和绑定的菜单条。一个frame窗口只可以有一个菜单条。如果你新创建了一个，那么旧的那个将被删除和释放。

GetTitle 和 SetTitle 函数用来访问窗口标题栏上的文本。

Iconize 函数使得frame窗口最小化或者从最小化状态恢复。你可以使用IsIconized函数检查当前的最小化状态。

Maximize 函数使得frame窗口最大化或者从最大化状态恢复。你可以用IsMaximized函数来检查当前的最大化状态。

SetIcon函数可以设置在frame窗口最小化的时候显示的图标。各个平台的窗口管理器也可能把这个图标用于别的用途，比如显示在任务条上或者在显示在窗口浏览器中。你还可以通过SetIcons函数指定一组不同颜色和深度的图标列表。

SetShape 函数用来给frame窗口设置特定的显示区域。目前支持这个函数的平台有Windows，Mac OSX和GTK+以及打开某种X11扩展功能的X11版本。

ShowFullScreen函数使用窗口在全屏和正常状态下切换，所谓全屏状态指的是frame窗口将隐藏尽可能多的修饰元素，而尽可能把客户区以最大化的方式显示。可以使用IsFullScreen函数来检测当前的全屏状态。

**不规则的Frame窗口**

如果你希望制作一个外貌奇特的应用程序，比如一个时钟程序或者一个媒体播放器，你可以给frame窗口指定一个不规则的区域，只有这个区域范围内的部分才会被显示出来。下图演示了一个没有任何标题栏，状态栏，菜单条的frame窗口，它的重画函数显示了一个企鹅，这个窗口被设置了一个区域使得只有这个企鹅才会显示。

![](images/0304.jpg)

显示一个有着奇特外观的程序的代码并不复杂，你可以在附带光盘的samples/shaped目录里找到一个使用不同图片的完整的例子。总的来说，当窗口被创建时，它加载了一幅图片，并且使用这幅图片的轮廓创建了一个区域。在GTK+版本上，设置窗口区域的动作必须在发送了窗口创建事件之后，所以你需要使用宏定义区别对待GTK+的版本。下面的例子演示了你需要怎样对事件表，窗口的构造函数和窗口的创建事件处理函数进行修改：

```c++
BEGIN_EVENT_TABLE(ShapedFrame, wxFrame)
    EVT_MOTION(ShapedFrame::OnMouseMove)
    EVT_PAINT(ShapedFrame::OnPaint)
#ifdef __WXGTK__
    EVT_WINDOW_CREATE(ShapedFrame::OnWindowCreate)
#endif
END_EVENT_TABLE()

ShapedFrame::ShapedFrame()
            :wxFrame((wxFrame *)NULL, wxID_ANY, wxEmptyString,
                     wxDefaultPosition, wxSize(250, 300),
                     wxFRAME_SHAPED | wxSIMPLE_BORDER | wxFRAME_NO_TASKBAR | wxSTAY_ON_TOP
                    )
{
    m_hasShape = false;
    m_bmp = wxBitmap(wxT("penguin.png"), wxBITMAP_TYPE_PNG);
    SetSize(wxSize(m_bmp.GetWidth(), m_bmp.GetHeight()));
#ifndef __WXGTK__
    // On wxGTK we can't do this yet because the window hasn't
    // been created yet so we wait until the EVT_WINDOW_CREATE
    // event happens. On wxMSW and wxMac the window has been created
    // at this point so we go ahead and set the shape now.
    SetWindowShape();
#endif
}
// Used on GTK+ only
void ShapedFrame::OnWindowCreate(wxWindowCreateEvent& WXUNUSED(evt))
{
    SetWindowShape();
}
```


为了创建一个区域模板，我们从一幅图片的创建了一个区域，其中的颜色参数用来指定需要透明显示的颜色，然后调用frame窗口的SetShape函数设置这个区域模板。

```c++
void ShapedFrame::SetWindowShape()
{
    wxRegion region(m_bmp, *wxWHITE);
    m_hasShape = SetShape(region);
}
```


为了让这个窗口可以通过鼠标拖拽来实现在窗口上的移动，我们可以通过下面的鼠标移动事件处理函数：

```c++
void ShapedFrame::OnMouseMove(wxMouseEvent& evt)
{
    wxPoint pt = evt.GetPosition();
    if (evt.Dragging() && evt.LeftIsDown())
    {
        wxPoint pos = ClientToScreen(pt);
        Move(wxPoint(pos.x - m_delta.x, pos.y - m_delta.y));
    }
}
```


而重画事件处理函数就比较简单了，当然在你的真实的应用程序里，你可以在其中放置更多的图形元素。

```c++
void ShapedFrame::OnPaint(wxPaintEvent& evt)
{
    wxPaintDC dc(this);
    dc.DrawBitmap(m_bmp, 0, 0, true);
}
```


你也可以参考wxWidgets的发行版中的samples/shaped目录中的例子。

**小型frame窗口**

在Window平台和GTK+平台上，你可以使用wxMiniFrame来实现那些必须使用小的标题栏的窗口，例如，来实现一个调色板工具。下图(译者注：这个图片应该是搞错了，是下一个例子中的图片，不过作者的翻译源使用的就是这个图片，没有办法了。)演示了windows平台上的这种小窗口的样子。而在Max Os X平台上，这个小窗口则是直接使用的普通的frame窗口代替。其它方面wxMiniFrame和wxFrame都是一样的。

![](images/0305.jpg)

**wxMDIParentFrame**

这个窗口类继承自wxFrame，是wxWidgets的MDI(多文档界面)体系的组成部分。MDI的意思是由一个父窗口管理零个或多个子窗口的一种界面架构。这种MDI界面结构依平台的不同而有不同的外观，下图演示了两种主要的外观。在windows平台上，子文档窗口是排列在其父窗口内的一组frame窗口，这些文档窗口可以被平铺，层叠或者把其中的某一个在父窗口的范围内最大化以便在某个时刻仅显示一个文档窗口。wxWidgets会自动在主菜单上增加一组菜单用来控制文档窗口的这些操作。MDI界面的一个优点是使得整个应用程序界面相对来说显得不那么零乱。这一方面是由于因为所有的文档窗口被限制在父窗口以内，另外一个方面，父窗口的菜单条会被活动的文档窗口的菜单条替代，这使得多个菜单条的杂乱性也有所减轻。

![](images/0306.jpg)

而在GTK+平台上，wxWidgets则通过TAB页面控件来模拟多文档界面。在某个时刻只能有一个窗口被显示，但是用户可以通过TAB页面在窗口之间进行切换。在Mac Os上，MDI的父窗口和文档窗口则都使用和普通的Frame窗口一样的外观，这符合这样的一个事实就是在Mac Os上，文档总是在一个新的窗口中被打开。

在那些MDI的文档窗口包含在父窗口之中的平台上，父窗口将把它的所有的文档窗口排列在一个子窗口中，而这个窗口可以和frame窗口中的其它控件子窗口和平共处。在上图中，父窗口将一个文本框控件和那些文档窗口进行了这样的排列。更详细的情形请参考wxWidgets发行版本的samples/mdi目录中的例子。

除了父窗口的菜单条外，每一个文档窗口都可以有自己的菜单条。当某个文档窗口被激活时，它的菜单条将显示在父窗口上，而没有子文档窗口被激活时，父窗口显示自己的菜单条。在构建子文档窗口的菜单条时，你应该主要要包含那些同样命令的父窗口的菜单项，再加上文档窗口自己的命令菜单项。父窗口和文档窗口也可以拥有各自的工具条和状态栏，但是这两者并没有类似菜单条这样的机制。

wxMDIParentFrame类的构造函数和wxFrame类的构造函数是完全一样的。

**wxMDIParentFrame的窗口类型**

wxMDIParentFrame类额外的窗口类型列举如下：

| 窗口类型               | 说明                                         |
| ---------------------- | -------------------------------------------- |
| wxFRAME_NO_WINDOW_MENU | 在Windows下，去掉通常自动添加的Window菜单 。 |

**wxMDIParentFrame的成员函数**

下面列出了wxMDIParentFrame类除了wxFrame的函数以外的主要成员函数。

ActivateNext和ActivatePrevious函数激活前一个或者后一个子文档窗口。

Cascade和Tile层叠或者平铺所有的子窗口。ArrangeIcons函数以图标的方式平铺所有最小化的文档窗口。这三个函数都只适用于Windows平台。

GetActiveChild获取当前活动窗口的指针(如果有的话)。

GetClientWindow函数返回那个包含所有文档窗口的子窗口的指针。这个窗口是自动创建的，但是你还是可以通过重载OnCreateClient函数来返回一个你自己的继承自wxMDIClientWindow的类的实例。如果你这样作，你就需要使用两步创建的方法创建这个多文档父窗口类。

**wxMDIChildFrame**

wxMDIChildFrame窗口应该总被创建为一个wxMDIParentFrame类型窗口的子窗口。正如我们前面已经提到的那样，依平台的不同，这种类型的窗口既可以是在其父窗口范围内的一组窗口列表，也有可能是在桌面上自由飞翔的普通的frame窗口。

除了父窗口必须不能为空，它的构造函数和普通的frame的构造函数是一样的。

```c++
#include "wx/mdi.h"
wxMDIParentFrame* parentFrame = new wxMDIParentFrame(NULL, ID_MYFRAME, wxT("Hello wxWidgets"));
wxMDIChildFrame* childFrame = new wxMDIChildFrame(parentFrame, ID_MYCHILD, wxT("Child 1"));
childFrame->Show(true);
parentFrame->Show(true);
```

**wxMDIChildFrame的窗口类型**

wxMDIChildFrame和wxFrame的窗口类型是一样的。尽管如此，不是所有的窗口类型的值在各个平台上都是有效的。

**wxMDIChildFrame的成员函数**

wxMDIChildFrame的除其基类wxFrame以外的主要成员函数列举如下：

Activate函数激活本窗口，将其带到前台并且使得其父窗口显示它的菜单条。

Maximize函数将其在父窗口范围内最大化(仅适用于windows平台)。

Restore函数使其从最大化的状态恢复为普通状态(仅适用于windows平台)。

**wxDialog**

对话框是一种用来提供信息或者选项的顶层窗口。他可以有一个可选的标题栏，标题栏上可以有可选的关闭或者最小化按钮，和frame窗口一样，你也可以给它指定一个图标以便显示在任务栏或者其它的地方。一个对话框可以组合任何多个非顶层窗口或者控件，举例来说，可以包含一个wxNoteBook控件和位于底部的两个确定和取消按钮。正如它的名字所指示的那样，对话框的目的相比较于整个应用程序的主窗口，只是为了给用户提示一些信息，提供一些选项等。

有两种类型的对话框： **模式的** 或者 **非模式的**。一个模式的对话框在应用程序关闭自己之前阻止其它的窗口处理来自应用程序的消息。而一个非模式的对话框的行为则更像一个frame窗口。它不会阻止应用程序中的别的窗口继续处理消息和事件。要使用模式对话框，你应该调用ShowModal函数来显示对话框，否则就应该象frame窗口那样，使用Show函数来显示对话框。

模式对话框是wxWindow派生类中极少数允许在堆栈上创建的对象之一，换句话说，你既可以使用下面的方法使用模式对话框:

```c++
void AskUser()
{
    MyAskDialog *dlg = new MyAskDialog(...);
    if ( dlg->ShowModal() == wxID_OK )
        ...
        dlg->Destroy();
}
```


也可以直接使用下面的方法：

```c++
void AskUser()
{
    MyAskDialog dlg(...);
    if ( dlg.ShowModal() == wxID_OK )
        ...
        //这里不需要调用Destroy()函数
}
```

通常你应该从wxDialog类派生一个你自己的类，以便你可以更方便的处理类似wxEVT_CLOSE这样的事件以及其它命令类型的事件。通常你应该在派生类的构造函数中创建你的对话框需要的其它控件。

和wxFrame类一样，如果你的对话框只有一个子窗口，那么wxWidgets会自动为你布局这个窗口，但是如果有超过一个子窗口，应用程序应该自己负责窗口的布局(参考第7章)

当你调用Show函数时，wxWidgets会调用InitDialog函数来发送一个wxInitDialog事件到这个窗口，以便开始对话框数据传输和验证以及其它的事情。

除了默认的构造函数，wxDialog还拥有下面的构造函数：

```c++
wxDialog(wxWindow* parent, wxWindowID id, const wxString& title,
         const wxPoint& pos = wxDefaultPosition,
         const wxSize& size = wxDefaultSize,
         long style = wxDEFAULT_DIALOG_STYLE,
         const wxString& name = wxT("dialog"));
```


例如:

```c++
wxDialog* dialog = new wxDialog(NULL, ID_MYDIALOG,
                                wxT("Hello wxWidgets"),
                                wxDefaultPosition,
                                wxSize(500, 300));
dialog->Show(true);
```


在调用Show(true)函数或者ShowModal函数之前，对话框都是不可见的，以便其可以以不可见的方式进行窗口布局。

默认情况下，如果对话框的父窗口为NULL，应用程序会自动指定其主窗口为其父窗口，你可以通过指定wxDIALOG_NO_PARENT类型来创建一个无父无母的孤儿dialog类，不过对于模式对话框来说，最好不要这样作。

和wxFrame类一样，最好不要直接使用delete操作符删除一个对话框，取而代之使用Destroy或者Close。以便在对话框的所有事件处理完毕以后才释放对话框，当你调用Cloes函数时，默认发送的wxEVT_CLOSE事件的处理函数其实也是调用了Destroy函数。

需要注意，当一个模式的对话框被释放的时候，它的某个事件处理函数需要调用EndModal函数，这个函数的参数是导致这次释放的那个事件所属窗口的窗口标识符(例如wxID_OK或者wxID_CANCEL)，这样才能使得应用程序退出这个模式对话框的事件处理循环，从而使得调用ShowModal的代码能够释放这个窗口，而这个标识符则作为ShowModal函数的返回值。我们举下面一个OnCancel函数为例：

```c++
//wxID_CANCEL的命令事件处理函数
void MyDialog::OnCancel(wxCommandEvent& event)
{
    EndModal(wxID_CANCEL);
}
//显示一个模式对话框
void ShowDialog()
{
    //创建这个对话框
    MyDialog dialog;
    // OnCancel函数将会在ShowModal执行的过程中被调用
    if (dialog.ShowModal() == wxID_CANCEL)
    {
        ...
    }
}
```


下图展示了几个典型的对话框，我们会在第9章阐述它们的创建过程。当然，对话框可以远比图中的那些更复杂，例如下面第二个图中的那样，它包含一个分割窗口，一个树形控件以便显示不同的面板(panel)，以及一个扮演属性编辑器的网格控件

![](images/0307.jpg)

![](images/0308.jpg)

**wxDialog的窗口类型**

除了基本窗口类型以外，wxDialog还有下面一些窗口类型可以使用：

| 窗口类型               | 说明                                                         |
| ---------------------- | ------------------------------------------------------------ |
| wxDEFAULT_DIALOG_STYLE | 其值等于wxSYSTEM_MENU \| wxCAPTION \| wxCLOSE_BOX。          |
| wxCAPTION              | 在对话框上显示标题。                                         |
| wxMINIMIZE_BOX         | 在标题栏显示最小化按钮。                                     |
| wxMAXIMIZE_BOX         | 在标题栏显示最大化按钮。                                     |
| wxCLOSE_BOX            | 在标题栏显示关闭按钮。                                       |
| wxSTAY_ON_TOP          | 对呼框总在最前。仅支持windows平台。                          |
| wxSYSTEM_MENU          | 显示系统菜单。                                               |
| wxRESIZE_BORDER        | 显示可变大小边框。                                           |
| wxDIALOG_NO_PARENT     | 如果创建对话框的时候父窗口为NULL，则应用程序会使用其主窗口作为对话框的父窗口，使用这个类型可以使得在这种情况下，对话框强制使用NULL作为其父窗口。模式窗口不推荐强制使用NULL作为父窗口。 |

下表解释了对话框类额外的扩展窗口类型。虽然wxWS_EX_BLOCK_EVENTS类型是窗口基类的窗口类型，但是由于它是对话框类的默认扩展类型，我们在这里也进行了描述。

| 窗口类型                | 说明                                                         |
| ----------------------- | ------------------------------------------------------------ |
| wxDIALOG_EX_CONTEXTHELP | 在Windows平台上，这个扩展类型使得对话框增加一个查询按钮。如果这个按钮被按下，对话框将进入一种帮助模式，在这种模式下，无论用户点击哪个子窗口，都将发送一个wxEVT_HELP事件。这个扩展类型不可以和wxMAXIMIZE_BOX和wxMINIMIZE_BOX类型混用。 |
| wxWS_EX_BLOCK_EVENTS    | 这是一个默认被设置的扩展类型。意思是阻止命令类型事件向更上一级的窗口传播。要注意调用SetExtraStyle函数可以重置这个扩展类型。 |
| wxDIALOG_EX_METAL       | 在Mac OS X平台上，这个扩展类型导致对话框使用金属外观。不要滥用这个类型，因为它默认用户的机器有各种多媒体硬件 |

**wxDialog事件**

下表解释了相对于窗口基类来说额外的对话框事件：

| 事件                  | 说明                                                         |
| --------------------- | ------------------------------------------------------------ |
| EVT_ACTIVATE(func)    | 用于处理wxEVT_ACTIVATE事件，在对话框即将激活或者去激活的时候产生。处理函数的参数类型是wxActivateevent. |
| EVT_CLOSE(func)       | 用户处理wxEVT_CLOSE事件，在应用程序或者操作系统即将关闭窗口的时候产生。处理函数的参数类型是wxCloseEvent，这个事件可以调用Veto函数来放弃事件的后续处理。 |
| EVT_ICONIZE(func)     | 用来处理wxEVT_ICONIZE事件，在窗口被最小化或者从最小化状态恢复的时候产生。处理函数的参数类型是wxIconizeeventc，调用IsIconized来检测到底是最小化还是从最小化恢复。 |
| EVT_MAXIMIZE(func)    | 用来处理wxEVT_MAXIMIZE事件，这个事件在窗口被最大化或者从最大化状态恢复的时候产生，处理函数的参数类型是wxMaximizeevent，调用IsMaximized函数确定具体的状态类型。 |
| EVT_INIT_DIALOG(func) | 用于处理wxEVT_INIT_DIALOG事件，在对话框初始化自己之前被产生。处理函数的参数类型是wxInitDialogevent，wxPanel也会产生这个事件。默认执行的操作是调用TransferDataToWindow。 |

**wxDialog的成员函数**

GetTitle和SetTitle用来操作对话框标题栏上的文本。

Iconize最小化或者从最小化状态恢复，你可以用IsIconized函数检测当前的最小化状态。

Maximize最大化或者从最大化状态恢复，使用IsMaximized函数检测窗口的最大化状态。仅适用于windows。

SetIcon设置对话框的图标，图标会在对话框被最小化的时候显示，也用于其它一些目的。你还可以调用SetIcons来给对话框设置一系列不同颜色和深度的图标。

ShowModal用来显示模式对话框，它将返回传递给EndModal函数的窗口标识符，通常是用户为了关闭这个对话框所点击的按钮的标识符。默认情况下(在对话框的wxEVT_CLOSE事件处理函数中)，关闭对话框导致一个wxID_CANCEL事件。这个事件默认的处理函数使用wxID_CANCEL参数调用EndModal函数。因此，如果你在对话框中放置了一个标识符为wxID_CANCEL的按钮，除非你还想作别的事情，否则默认的逻辑对于这个按钮来说就足够了。

SetLeftMenu和SetRightMenu函数只适用于Microsoft的智能手机系统，用来设置左右菜单按钮的命令，参数为一个命令标识符，例如ID_OK，一个标签以及一个指向wxMenu的用于显示的菜单指针。

**wxPopupWindow**

弹出窗口不支持所有的平台(至少目前还不支持Max OS X)，所有我们只是简略的介绍一下。

弹出窗口是一种顶层窗口，它有较小的修饰栏以用来实现那些生命期很短暂的窗口，例如菜单或者工具提示。创建的方法是调用其构造函数，指定父窗口和可选的类型(默认类型是wxNO_BODER)，然后设置合适的大小和位置，以便它可以在屏幕上显示出来。

wxPopupTransientWindow是一种特殊的wxPopupWindow，它在自己失去焦点时，或者鼠标在它的窗口范围以外点击时，或者自己的Dismiss函数被调用时自动释放自己。


