### 窗口布局基础

在深入介绍窗口布局之前，我们先来大概了解一下什么时候你需要进行窗口布局以及你拥有的几种选择。  
  
一个最简单的情况是你拥有一个frame窗口，其中只有一个客户窗口位于这个frame的客户区。这是最简单的情况，wxWidgets将为你完成所有的布局工作，将那个客户区窗口缩放到刚好适合frame的客户区的大小。wxWidgets也会管理frame的菜单条，工具条和状态条(如果有的话)。当然，如果你想使用两个工具条，你至少要管理其中的一条，而如果frame窗口的客户区拥有超过一个窗口，你将不得不自己单独管理它们所有的大小和位置，比如你可能需要在OnSize事件中计算每一个窗口的大小并且设置它们的新位置。当然，你也可以使用窗口布局控件。类似的，如果你创建了一个定制的控件，这个控件拥有多个子窗口，你也需要安排这些子窗口的布局以便当你的这个控件被别人使用而且默认大小改变的时候，对那些子窗口进行合理的布局。  
  
另外，大多数应用程序都需要创建自己的对话框，有时候有些程序会创建很多个定制的对话框，这些对话框可能被改变大小，在这种情况下，对话框上所有的控件的大小也应该发生相应的改变，以便即使这个对话框已经比最初设计的时候大了很多，这些控件看上去也不会显得很奇怪。另外应用程序的语言也可能改变，某些在默认语言中很短的标签，在另外一种语言中可能会变得很长。如果你的应用程序要应付成百上千中这种对话框，相信即使使用窗口布局控件，维护它们也是一个令人望而生畏的工作，还好幸运的是我们还可以使用一些工具让所有这些事情变得不那么恐怖甚至还有一点点的乐趣在其中。  
  
如果你选择使用布局控件，你需要自己决定怎样创建和发布它们。你可以自己写或者使用工具来创建C++或者其它语言的代码，或者你可以直接使用XRC文件，XRC文件用来将布局的定义保存在一个Xml文件中，可以被应用程序动态加载，也可以通过wxrc工具将其编译成C++源文件以便和别的源文件编译成一个单独的可执行文件。大多数的wxWidgets对话框编辑器都既支持产生C++的代码，又支持生成XRC文件。怎样作决定完全取决于你自己的审美观，有些人喜欢把一切都放在一个C++代码中以保持足够的灵活性，而另外一些人则喜欢把实现功能的代码和产生界面的代码分开。  
  
接下来的小节用来描述布局控件的原理，再往后一个小节则用来描述怎样使用各种具体的布局控件来编程。  
  

