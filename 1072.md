### wxWidgets中图片相关的类

wxWidgets支持四种和位图相关的类：wxBitmap，wxIcon，wxCursor和wxImage。  

**wxBitmap** 是一个平台有关的类，它拥有一个可选的wxMask属性以支持透明绘画。在windows系统上，wxBitmap是通过设备无关位图(DIBs)实现的，而在GTK+和X11平台上，每个wxBitmap则包含一个GDK的pixmap对象或者X11的pixmap对象。而在Mac平台上，则使用的是PICT。wxBitmap可以和wxImage进行互相转换。  

**wxIcon**
用来实现各个平台上的图标，一个图标指的是一个小的透明图片，可以用来代表不同的frame或者对话框窗口。在GTK+，X11和Mac平台上，icon就是一个小的总含有wxMask的wxBitmp，而在windows平台上，wxIcon则是封装了HICON对象。  

**wxCursor**
则是一个用来展示鼠标指针的图像，在GTK+平台上是用的GdkCursor，X11和Mac平台上用的是各自的Cursor，而在windows平台上则使用的是HCURSOR。wxCursor有一个热点的概念(所谓热点指的是图片中用来精确代表指针单击位置的那个点)，也总是包含一个遮罩(mask)。  

**wxImage**
则是四个类中唯一的一个平台无关的实现，它支持24bit位图以及可选的alpha通道。wxImage可以从wxBitmap类使用wxBitmap::ConvertToImage函数转换而来，也可以从各种各样的图片文件中加载，它所支持的图片格式也是可以通过图片格式处理器来扩展的。它拥有操作其图片上某些bit的能力，因此也可以用来对图片进行一个基本的操作。和wxBitmap不同，wxImage不可以直接被设备上下文wxDC使用，如果要在wxDC上绘图，需要现将wxImage转换成wxBitmap，然后就可以使用wxDC的DrawBitmap函数进行绘图了。wxImage支持设置一个掩码颜色来实现透明的效果，也支持通过alpha通道实现非常复杂的透明效果。  

你可以在这些图片类型之间进行相互转换，尽管某些转换操作是平台相关的。  

注意图片类中大量使用引用记数器，因此对图片类进行赋值和拷贝的操作的系统开销是非常小的，不过这也意味着对一个图片的更改可能会影响到别的图片。  

所有的图片类都使用下表列出的标准的wxBitmapType标识符来读取或者保存图片数据：  

| 类型                             | 说明                                                         |
| -------------------------------- | ------------------------------------------------------------ |
| wxBITMAP_TYPE_BMP                | Windows位图文件 (BMP)。                                      |
| wxBITMAP_TYPE_BMP_RESOURCE       | 从windows可执行文件资源部分加载的Windows位图。               |
| wxBITMAP_TYPE_ICO                | Windows图标文件(ICO)。                                       |
| wxBITMAP_TYPE_ICO_RESOURCE       | 从windows可执行文件资源部分加载的Windows图标。               |
| wxBITMAP_TYPE_CUR                | Windows光标文件(CUR)。                                       |
| wxBITMAP_TYPE_CUR_RESOURCE       | 从windows可执行文件资源部分加载的Windows光标。               |
| wxBITMAP_TYPE_XBM                | Unix平台上使用的XBM单色图片。                                |
| wxBITMAP_TYPE_XBM_DATA           | 从C++数据中构造的XBM单色位图。                               |
| wxBITMAP_TYPE_XPM                | XPM格式图片，最好的支持跨平台并且支持编译到应用程序中去的格式。 |
| wxBITMAP_TYPE_XPM_DATA           | 从C++数据中构造的XPM图片。                                   |
| wxBITMAP_TYPE_TIF                | TIFF格式位图，在大图片中使用比较普遍。                       |
| wxBITMAP_TYPE_GIF                | GIF格式图片，最多支持256中颜色，支持透明。                   |
| wxBITMAP_TYPE_PNG                | PNG位图格式，一个使用广泛的图片格式，支持透明和alpha通道，没有版权问题。 |
| wxBITMAP_TYPE_JPEG               | JPEG格式位图，一个广泛使用的压缩图片格式，支持大图片，不过它的压缩算法是有损耗压缩，因此不适合对图片进行反复加载和压缩。 |
| wxBITMAP_TYPE_PCX                | PCX图片格式。                                                |
| wxBITMAP_TYPE_PICT               | Mac PICT位图。                                               |
| wxBITMAP_TYPE_PICT_RESOURCE      | 从可执行文件资源部分加载的Mac PICT位图。                     |
| wxBITMAP_TYPE_ICON_RESOURCE      | 仅在Mac OS X平台上有效，用来加载一个标准的图标(比如wxICON_INFORMATION)或者一个图标资源。 |
| wxBITMAP_TYPE_ANI                | Windows动画图标(ANI)。                                       |
| wxBITMAP_TYPE_IFF                | IFF位图文件。                                                |
| wxBITMAP_TYPE_MACCURSOR          | Mac光标文件。                                                |
| wxBITMAP_TYPE_MACCURSOR_RESOURCE | 从可执行文件资源部分加载的Mac光标。                          |
| wxBITMAP_TYPE_ANY                | 让加载图片的代码自己确定图片的格式。                         |

  

