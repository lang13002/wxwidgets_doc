### 字符串类型

使用字符串类来代替标准的字符串指针的好处是被普遍接受的。而wxWidgets就提供了它自己的字符串类：wxString，无论在wxWidgets内部还是在其提供的API接口上，这个类都被很广泛的使用。wxString类拥有你对一个字符串类期待的所有的操作，包括：动态内存管理，从其它字符串类型构建，赋值操作，单个字符访问，字符串连接和比较，子字符串获取，大小写转换，空格字符的修剪和补齐，查找和替换，类似C语言printf的操作以及类似流一样的插入函数等等。  

除了上述的这些字符串处理常用功能，wxString还支持一些额外的特性。wxString完美支持Unicode，包括ANSI字符和Unicode的互相转换，这个特性是和wxWidgets的编译配置无关的。使用wxString还使得你的代码拥有直接将字符串传递给库函数以及直接从库函数返回字符串的能力。另外，wxString已经实现了90%的STL中的std::string类的函数，这意味着对STL熟悉的用户基本上不需要重新学习wxString的使用方法。  

**使用wxString**  

在你的应用程序中使用wxString类型是非常简单而直接的。将你程序中使用std::string或者是别的你习惯的字符串类的地方，全部用wxString代替基本就可以了。要注意的是，所有参数中使用字符串的地方，最好使用const wxString&这样的声明(这使得函数内部对于字符串的赋值操作由于使用了引用记数器技术而变得更快速)，而所有返回值中使用的字符串则最好直接使用wxString类型，这使得在函数内部即使返回一个局部变量也是安全的。  

C和C++的程序员通常都已经很熟悉字符串的各种操作了，因此wxString的详细的API参考就不在这里罗嗦了，请参考wxWidgets的相关文档。  

你可能会注意到wxString的好多函数具有同样的功能，比如Length，Len和length函数都返回这个字符串的长度。在这种情况下，你最好使用标准STL兼容的函数形式。这会让你的代码对别的程序员来说更亲切，并且将使你的代码更容易转换为别的不使用wxWidgets库的代码，你甚至可以直接使用typedef将wxString重定义为std::string。另外wxWidgets某一天可能会开始使用标准的std::string，因此这种作法也会让你的代码更容易保持向前兼容。(当然，wxString的函数也会保留以保证向后兼容。)  

**wxString，字符以及字符串常量**  

wxWidgets定义了一个wxChar类型，在不同的编译选项(ANSI或Unicode)下，这个类型用来映射char类型或者wchar_t类型。象前面提到的那样，你不必使用单独的char类型或者wchar_t类型，wxString内部存储数据的时候使用的是相应的C类型。在任何时候，如果你需要对单个字符进行操作，你应该使用wxChar类型，这将使得你的代码在ANSI版本和Unicode版本中保持一致，而不必使用大量的预定义宏。  

如果wxWidgets被编译成Unicode的版本，和标准字符串常量是不兼容的，因为标准字符串常量无论在哪种版本中都是char*类型。如果你想在Unicode版本中直接使用字符串常量，你应该使用一个转义宏L。wxWidgets提供了一个宏wxT(或者_T)来封装字符串常量，这个宏在ANSI版本中被定义为什么事情也不做，而在Unicode版本中则用来封装L宏，因此无论在哪种版本中，你都可以使用下面的方法使用字符串常量：  

```c++
wxChar ch = wxT('*');
wxString s = wxT("Hello, world!");
wxChar* pChar = wxT("My string");
wxString s2 = pChar;
```


关于使用Unicode版本的更详细的信息，请参考第16章:"编写国际化应用程序"。  

**wxString到C指针的转换基础**  

因为有时候你需要直接以C类型访问wxString的内部数据进行底层操作，wxWidgets提供了几种对应的访问方法：  


  * mb_str函数无论在ANSI版本还是Unicode版本都返回一个const char*类型的指针const char*，如果是Unicode版本，则字符串首先经过转换，转换过程可能导致数据丢失。
  * wc_str函数无论在ANSI版本还是Unicode版本都返回一个wchar_t*类型，如果是ANSI版本，则字符串首先被转换成Unicode版本然后再返回。
  * c_str则返回一个指向内部数据的指针 (ANSI版本为const char*类型，Unicode版本为const wchar_t*类型)。不进行任何转换。


你可以使用c_str函数的特性实现wxString和std::string之间的转换，如下所示:  

```c++
std::string str1 = wxT("hello");
wxString str2 = str1.c_str();
std::string str3 = str2.c_str();
```

使用wxString经常遇到的一个陷井是过度使用对const char*类型的隐式的类型强制转换。我们建议你在任何需要使用这种转换的时候，显式使用c_str来指明这种转换，下面的代码演示了两个常见的错误：  

```c++
// 这段代码将输入的字符串转换为大写函数，然后将其打印在屏幕上
// 并且返回转换后的值 (这是一个充满bug的代码)
const char *SayHELLO(const wxString& input)
{
    wxString output = input.Upper();
    printf("Hello, %s!\n", output);
    return output;
}
```

上面这四行代码有两个危险的缺陷，第一个是对printf函数的调用。在类似puts这样的函数中，隐式的类型强制转换是没有问题的，因为puts声明其参数为const
char*类型，但是对于printf函数，它的参数采用的是可变参数类型，这意味着上述printf代码的执行结果可能是任何一个种结果(包括正确打印出期待结果)，不过最常见的一种结果是程序异常退出，因此，应该使用下面的代码代替上面的printf语句：  

```c++
printf(wxT("Hello, %s!\n"), output.c_str());
```

第二个错误在于函数的返回值。隐式类型强制转换又被使用了，因为这个函数的返回值是const char*类型，这样的代码编译是没有问题的，但是它返回的将是一个局部变量的内部指针，而这个局部变量在函数返回以后就很快被释放了，因此返回的指针将变成一个无效指针。解决的方法很简单，应该将返回类型更改为wxString类型，下面列出了修改了以后的代码：  

```c++
// 这段代码将输入的字符串转换为大写函数，然后将其打印在屏幕上
// 并且返回转换以后的值 (这是正确的代码)
wxString SayHELLO(const wxString& input)
{
    wxString output = input.Upper();
    printf(wxT("Hello, %s!\n"), output.c_str());
    return output;
}
```


**标准C的字符串处理函数**  

因为大多数的应用程序都要处理字符串，因此标准C提供了一套相应的函数库。不幸的是，它们中的一部分是有缺陷的(比如strncpy函数有时候不会添加结束符NULL)，另外一部分则可能存在缓冲区溢出的危险。而另一方面，一些很有用的函数却没能够进入标准的C函数库。这些都是为什么wxWidgets要提供自己的额外的全局静态函数的原因，wxWidgets的一些静态函数视图避免这些缺陷：wxIsEmpty函数增加了对字符串是否为NULL的校验，在这种情况下也返回True。

wxStrlen函数也可以处理NULL指针，而返回0。wxStricmp函数则是一个平台相关的大小写敏感字符串比较函数，它在某些平台上使用stricmp函数而在另外一些平台上则使用strcasecmp函数。  

"wx/string.h"头文件中定义了wxSnprintf函数和wxVsnprintf，你应该使用它们代替标准的sprintf函数以避免一些sprintf函数先天的危险。带"n"的函数使用了snprintf函数，这个函数在可能的时候对缓冲区进行大小检查。你还可以使用wxString::Printf而不必担心遭受可能受到的针对printf的缓冲区溢出攻击。  

**和数字的相互转换**  

应用程序经常需要实现数字和字符串之间的转换，比如将用户的输入转换成数字或者将计算的结果显示在用户界面上。  

ToLong(long* val, int base=10)函数可以将字符串转换成一个给定进制的有符号整数。它在成功的时候返回True并将结果保存在val中，如果返回值是False，则表明字符串不是一个有效的对应的进制的数字。指定的进制必须是2到36的整数，0意味着根据字符串的前导符决定：0x开头的字符串被认为是16进制的，0-则被认为是8进制的，其它情况下认为是10进制的。  

ToULong(unsigned long* val, int base=10)的工作模式和ToLong函数一致，不过它的转换结果为无符号类型。  

ToDouble(double* val)则实现字符串到浮点数的转换。返回值为Bool类型。  

Printf(const wxChar* pszFormat, ...)和C语言的sprintf函数类似，将格式化的文本作为自己的内容。返回值为填充字符串的长度。  

静态函数Format(const wxChar* pszFormat, ...)则将格式化的字符串作为返回值。因此你可以使用下面的代码：  

```c++
int n = 10;
wxString s = "Some Stuff";
s += wxString::Format(wxT("%d"), n );
```


操作符"<<"可以用来在wxString中添加一个int，float或者是double类型的值。  

**wxStringTokenizer**  

wxStringTokenizer帮助你将一个字符串分割成几个小的字符串，它被用类代替和扩展标准C函数strtok，它的使用方法是：传递一个字符串和一个可选的分割符(默认为空白符)，然后循环调用GetNextToken函数直到HasMoreTokens返回False，如下所示:  

```c++
wxStringTokenizer tkz(wxT("first:second:third:fourth"), wxT(":"));
while ( tkz.hasMoreTokens() )
{
    wxString token = tkz.GetNextToken();
    // 处理单个字符串
}
```


默认情况下，wxStringTokenizer对于全空字符串的处理和strtok的处理相同，但是和标准函数不同的是，如果分割符为非空字符，它将把空白部分也作为一个子字符串返回。这对于处理那些格式化的表格数据(每一行的列数相同但是单元格数据可能为空)是比较有好处的，比如使用tab或者逗号作为分割符的情况。  

wxStringTokenizer的行为还受最后一个参数的影响，相关的描述如下：  


  * wxTOKEN_DEFAULT: 如前所述的默认处理方式; 如果分割符为空白字符则等同于wxTOKEN_STRTOK，否则等同于wxTOKEN_RET_EMPTY。
  * wxTOKEN_RET_EMPTY: 在这种模式，空白部分将作为一个子字符串部分被返回，例如"a::b:"如果用":"分割则返回三个子字符串a，""和b。
  * wxTOKEN_RET_EMPTY_ALL: 在这种模式下，最后的空白部分也将作为一个子字符串返回。这样"a::b:"使用":"分割将返回四个子字符串，其三个和wxTOKEN_RET_EMPTY返回的相同，最后一个则为一个""。
  * wxTOKEN_RET_DELIMS: 在这种模式下，分割符也作为子字符串的一部分(除了最后一个子字符串，它是没有分割符的)，其它方面类似wxTOKEN_RET_EMPTY。
  * wxTOKEN_STRTOK: 这种情况下，子字符串的产生结果和标准strtok函数完全相同。空白字符串将不作为一个子字符串。


wxStringTokenizer还有下面两个有用的成员函数：  


  * CountTokens函数返回分割完的子字符串的数目。
  * GetPosition返回某个位置的子字符串。

  

**wxRegEx**  

wxRegEx类用来实现正则表达式。这个类支持的操作包括正则表达式查找和替换。其实现方式有基于系统正则表达式库(比如现代的类Unix系统以及Mac OSX支持的POSIX标准正则表达式库)或者基于由Henry Spencer提供的wxWidgets内建库。POSIX定义的正则表达式有基础和扩展两套版本。内建的版本支持这两种模式而基于系统库的版本则不支持扩展模式。  

即使是对于那些支持正则表达式库的系统，wxWidgets默认的Unicode版本也采用了内建的正则表达式版本，ANSI版本则使用系统提供的版本。记住只有内建版本的正则表达式库才能完全支持Unicode。当编译wxWidgets的时候，覆盖这种默认设置是被允许的。如果在使用系统正则表达式库的Unicode版本中，在使用对应函数的之前，表达式和要匹配的数据都将被转换成8-bit编码的Unicode方式。  

使用wxRegEx的方法和其它所有使用正则表达式的方法没有区别。因为正则表达式的内容较为罗嗦而且又鉴于正则表达式的只在特定情况下使用，请参考wxWidgets手册中的相关内容了解具体的API。

  

