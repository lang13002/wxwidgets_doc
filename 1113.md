### 使用wxModule

wxWidgets的模块管理系统是一个很简单的系统，它允许应用程序(以及wxWidgets自己)可以定义将被wxWidgets自动在开始和退出时执行的初始化和资源清理代码。这有助于避免应用程序在OnInit函数和OnExit函数中依它们功能的需要添加过多的代码。  

要定义一个这样的模块，你需要实现一个wxModule的派生类，重载其OnInit和OnExit函数，然后在其声明部分使用DECLARE_DYNAMIC_CLASS宏，在其实现部分使用IMPLEMENT_DYNAMIC_CLASS宏(它们可以位于同一个文件内)。在系统初始化的时候，wxWidgets会找到所有wxModule的派生类，创建一个它的实例然后执行其OnInit函数，而在系统退出时执行其OnExit函数。  

举例如下:    

```c++
// 下面这个模块用来自动进行DDE的初始化和清除动作。
class wxDDEModule: public wxModule
{
DECLARE_DYNAMIC_CLASS(wxDDEModule)
public:
    wxDDEModule() {}
    bool OnInit() { wxDDEInitialize(); return true; };
    void OnExit() { wxDDECleanUp(); };
};
IMPLEMENT_DYNAMIC_CLASS(wxDDEModule, wxModule)
```



  

