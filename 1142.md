### 文档/视图框架的其它能力

上一节中我们通过一个简单的例子演示了使用文档/视图框架所必须的一些步骤，这一节我们来讨论这个框架中一些更深入的话题。  

**标准标识符**  

文档/视图系统支持很多默认的标识符，比如wxID_OPEN，wxID_CLOSE，wxID_CLOSE_ALL，wxID_REVERT，wxID_NEW，wxID_SAVE，wxID_SAVEAS，wxID_UNDO，wxID_REDO，wxID_PRINT和wxID_PREVIEW，为了更大的发挥框架的威力，你应该尽可能在你的菜单或者工具栏中使用这些标准的标识符。这些标识符的处理函数大多已经在wxDocManager类中实现，比如OnFileOpen，OnFileClose和OnUndo等。对应的处理函数将自动调用当前文档相应的处理函数。如果你愿意，你当然可以在你的frame窗口类或者wxDocManager的派生类中重载这些处理函数，不过通常都没有这个必要。  

**打印和打印预览**  

默认情况下，wxID_PRINT和wxID_PREVIEW使用标准的wxDocPrintout类来实现打印和打印预览，以便直接重用wxView::OnDraw函数。然而，这种用法的一个最大的缺陷是仅适用于只有一页的文档的情形。因此你可以创建你自己的wxPrintout类来重载标准的wxID_PRINT和wxID_PREVIEW处理，最快速的方法当然是使用wxHtmlEasyPrinting类，我们在第12章，"高级窗口类"的"HTML打印"小结有比较详细的介绍。  

**文件访问历史**  

当你的应用程序初始化的时候，可以直接通过wxConfig对象，使用wxDocManager::FileHistoryLoad函数在文件菜单的最下方加载一个文件访问历史列表，也可以在应用程序退出之前使用wxDocManager::FileHistorySave函数保存这个列表。比如，要加载文件访问历史，你可以这样做：  

```c++
// 加载文件访问历史
wxConfig config(wxT("MyApp"), wxT("MyCompany"));
config.SetPath(wxT("FileHistory"));
m_docManager->FileHistoryLoad(config);
config.SetPath(wxT("/"));
```


如果你是在创建主窗口或者其主菜单之前加载的文件访问历史，你可以显式的通过wxDocManager::FileHistoryAddFilesToMenu函数将其增加在菜单中。  

你也可以通过wxFileHistory类或者你自己的方法来实现文件访问历史功能，比如有时候你可能需要为每个文档窗口实现不同的文件访问历史。  

**显式创建文档类**  

有时候你需要显式的创建一个文档对象，比如说，有时候你想打开上次显式的文档，你可以通过下面的方式直接打开一个已经存在的文档：  

```c++
wxDocument* doc = m_docManager->CreateDocument(filename, wxDOC_SILENT);
```

或者象下面这样创建一个新的文档：    

```c++
wxDocument* doc = m_docManager->CreateDocument(wxEmptyString, wxDOC_NEW);
```

无论是上面哪种情况，都将自动创建一个相应的视图对象。  

