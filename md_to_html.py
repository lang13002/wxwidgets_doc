import codecs, markdown
from markdown.extensions import Extension
from markdown.extensions import extra

from markdown.extensions import legacy_attrs
from markdown.extensions import meta
from markdown.extensions import nl2br
from markdown.extensions import smarty
from markdown.extensions import toc


def main():
    body_pre = """<!DOCTYPE html>
<html lang="zh-CN" > 
<head>  
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" /> 
<link rel="stylesheet" href="styles/wx.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="styles/shCore.css" />
<link rel="stylesheet" type="text/css" href="styles/shCoreDefault.css" />
<link rel="stylesheet" type="text/css" href="styles/shThemeDefault.css" />
<script type="text/javascript" src="scripts/shCore.js"></script>
<script type="text/javascript" src="scripts/shBrushCpp.js"></script>
<script type="text/javascript">
    SyntaxHighlighter.all()
</script>
<title>wxWidgets跨平台GUI开发</title>   
</head> 
<body> 
<div id="doc-contents">
"""

    body_post = """
    </div>
<table align='center'  style='width: 80%; border:none'>
<script language="javascript" type="text/javascript">
var pages=document.URL;
var pagep=pages.lastIndexOf("\/");
if (pagep<1)
{
    pagep=pages.lastIndexOf("\/");
}
var pagec = pages.substring(pagep+1);
pagep=pagec.lastIndexOf(".");
var pagebare = pagec.substring(0, pagep) ;
var pagepre = pagebare - 1 ;
var pagenext = pagebare - 0 + 1 ;
if (pagepre%10000!=0)
{
    document.write("<td align='right' style='width:33%;border:none'><a href="+pagepre+".html><font color=blue>上一页</font></a>") ;
} else {
    document.write("<td align='center' style='width:33%;border:none'>");
}   
document.write("</td><td align='center' style='width:34%;border:none'><a href=index.html><font color=blue>目录</font></a>");    
document.write("</td><td align='left' style='width:33%;border:none'><a href="+pagenext+".html><font color=blue>下一页</font></a></td></tr>");
</script>
</table>          
</body>
</html>"""
    # 读取 markdown 文本
    input_file = codecs.open("README.md", mode="r", encoding="utf-8")
    text = input_file.read()

    # 转为 html 文本
    html = markdown.markdown(text, extensions=['extra', 'legacy_attrs', 'meta', 'nl2br', 'smarty', 'toc'])

    # 保存为文件
    output_file = codecs.open("index.html", mode="w", encoding="utf-8")
    output_file.write(body_pre)
    output_file.write(html)
    output_file.write(body_post)
        
    for id in range(1001, 1157):
        # 读取 markdown 文本
        input_file = codecs.open("{}.md".format(id), mode="r", encoding="utf-8")
        text = input_file.read()

        # 转为 html 文本
        html = markdown.markdown(text, extensions=['extra', 'legacy_attrs', 'meta', 'nl2br', 'smarty', 'toc'])
        html = html.replace("<p><strong>", "<p style='padding: 5px'></p><p><strong>");

        # 保存为文件
        output_file = codecs.open("{}.html".format(id), mode="w", encoding="utf-8")
        output_file.write(body_pre)
        output_file.write(html)
        output_file.write(body_post)
    
if __name__ == "__main__":
    main()